
let trainer = {
	name: "Misty",
	age: "21",
	pokemon: ["Psyduck", "Snorlax","Jigglypuff", "Squirtle"],
	friends: {
		hoen: ["Serena", "Lance"],
		kanto: ["Hilda", "Diantha"]
	},
	talk: function(){
		console.log(trainer.pokemon[2] + '! I choose you!');
	}
};
console.log(trainer);

console.log("Result of dot notation:");

console.log(trainer.name);

console.log("Result of square bracket notation:");

console.log(trainer.pokemon);

console.log("Result of talk method:");

console.log();



let pokemon = {
	name: "Psyduck",
	level: 4,
	health: 99.99,
	attack: 80,
	tackle: function (){
		console.log("This pokemon tackled targetPokemon")
		console.log("targetPokemon's health is now reduced to targetPokemonHealth")
	}
};


function Pokemon(name, level){
	this.name = name,
	this.level = level,
	this.health = 5*level,
	this.attack = 1.5*level
	this.tackle = function(target){
		console.log(this.name + " tackled " + target.name);
		console.log("targetPokemon's health is now reduced to targetPokemonHealth");
	}
	this.faint = function(){
		console.log(this.name + " fainted.")
	}
};
let Psyduck = new Pokemon ("Psyduck", 15);
let Snorlax = new Pokemon("Snorlax", 30);
let Jigglypuff = new Pokemon("Jigglypuff", 20);
let Squirtle = new Pokemon("Squirtle", 25);


console.log(Psyduck);
console.log(Snorlax);
console.log(Jigglypuff);
console.log(Squirtle);